# Interacting with Google Map API

Project Description:
This solution maps addresses on a google map.  I leverage the Google API to:

* Calculate coordinates to every address entered and save them in FileMaker fields (See Note 1).
* Pass coordinates in a found set to the Google API.  
* The Google API returns a HTML/javascript based map with my coordinates drawn onto the map.
* Finally, a callback event is also set on the google map to call FileMaker using the fmp protocol.   

Note 1: You might be wandering: why am I getting the coordinates when the address is entered instead of calculating them when the Google map API is called? Well, It comes down to performance.  It is faster for google to “draw” the map when you pass coordinates only instead of whole addresses.  If your found set is large, performance is key to faster “drawing”.  

I tried to write scripts as modular as possible, so it is easy to implement into your solution.

Pre-requisites:
*  This project was designed using FileMaker 16.  Some functionality might not work properly if you try opening this file in a lower FileMaker version.
*  You have to enable the “Allow URLs to perform FileMaker scripts” option. This option allows the web viewer to interact with the solution.  To enable this option, go to File->Manage->Security->Extended Privileges then click on “fmurlscript” and finally click on the Edit… button.  Set the user access to this option and you are done.

About field names:
* Field name starting with…	Denotes… 
* c_ 						    Calculation field
* z_						    Back end field, used mainly by developer and back-end procedures
* g_						    Global field

About HTMS/JS code:
There are 3 fields associated with the html/js code:
* map_code: Contains the js code without any FileMaker generated data or callbacks. 
* g_map_code: Contains the js code with all the FileMaker generated data and callbacks.
* map_code_file: Contains the html/js file.  As you know, once text is entered into a field (map_code) some formatting is lost. I included the file mainly for indentation purposes, so it is easy to update in any code editor.

if you want to know more about the Google API or if you want to make enhancements to the code I am including in this project, you can visit..

https://developers.google.com/maps/documentation/javascript/tutorial

Any feedback or comments are greatly appreciated.

Thanks,

Ed Gar
